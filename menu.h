
              // file is included in more than one place
#define WIN32_LEAN_AND_MEAN
#ifndef MENU_H                 // Prevent multiple definitions if this 
#define MENU_H 


class Menu;

#include "graphics.h"
#include "constants.h"
#include "textDX.h"
#include <string>
#include <sstream>
#include "input.h"
#include "submenu.h"
#include <time.h>
#include <vector>
#include "textureManager.h"
#include "image.h"

namespace menuNS
{ }

// inherits from Entity class
class Menu 
{
private:
	TextureManager checkTM;
	Image checkTexture;
	TextureManager checkTM2;
	Image checkTexture2;
	TextureManager ControllsScreenTM;
	Image ControllsScreen;
   TextDX *menuItemFont;
   TextDX *menuItemFontHighlight;
   TextDX *menuHeadingFont;

   TextDX *submenuItemFont;
   TextDX *submenuItemFontHighlight;
   TextDX *submenuHeadingFont;

   Input   *input;         // pointer to the input system
   Graphics *graphics;
   int selectedItem;
   int subselectedItem;

   std::string chosen;
   std::string menuHeading;
   std::string menuItem1;
   std::string menuItem2;
   std::string menuItem3;

   std::string FirstsubmenuHeader;
   std::string Firstsubmenu1;
   std::string Firstsubmenu2;
   std::string Firstsubmenu3;

   std::string SecondsubmenuHeader;
   std::string Secondsubmenu1;
   std::string Secondsubmenu2;
   std::string Secondsubmenu3;

   std::string ThirdsubmenuHeader;
   std::string Thirdsubmenu1;
   std::string Thirdsubmenu2;
   std::string Thirdsubmenu3;

   D3DXVECTOR2 menuAnchor;
   int verticalOffset;
   int linePtr;
   int sublinePtr;
   COLOR_ARGB highlightColor ;
   COLOR_ARGB normalColor;
   bool upDepressedLastFrame;
   bool downDepressedLastFrame;
   bool controls;

     std::string currentMenu;

   std::vector<std::string> mainMenu;
   std::vector<std::string> optionsMenu;
   std::vector<std::string> cheatCodesMenu;
   std::vector<std::string> lifeStoryMenu;


   enum posMenu {first, second, third, notChosen} posMenu;
public:
    // constructor
    Menu();
	void initialize(Graphics *g, Input *i);
	void update();
	int getSelectedItem() {return selectedItem;}
	void displayMenu();
	bool playGame;
	bool infiniteLives;
	bool skipLevel;
	bool onControls() {return controls;}
};
#endif

