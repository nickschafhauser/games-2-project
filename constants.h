// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 constants.h v1.1

#ifndef _CONSTANTS_H            // Prevent multiple definitions if this 
#define _CONSTANTS_H            // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include <windows.h>

//=============================================================================
// Function templates for safely dealing with pointer referenced items.
// The functions defined by these templates may be called using a normal
// function call syntax. The compiler will create a function that replaces T
// with the type of the calling parameter.
//=============================================================================
// Safely release pointer referenced item
template <typename T>
inline void safeRelease(T& ptr)
{
    if (ptr)
    { 
        ptr->Release(); 
        ptr = NULL;
    }
}
#define SAFE_RELEASE safeRelease            // for backward compatiblility

// Safely delete pointer referenced item
template <typename T>
inline void safeDelete(T& ptr)
{
    if (ptr)
    { 
        delete ptr; 
        ptr = NULL;
    }
}
#define SAFE_DELETE safeDelete              // for backward compatiblility

// Safely delete pointer referenced array
template <typename T>
inline void safeDeleteArray(T& ptr)
{
    if (ptr)
    { 
        delete[] ptr; 
        ptr = NULL;
    }
}
#define SAFE_DELETE_ARRAY safeDeleteArray   // for backward compatiblility

// Safely call onLostDevice
template <typename T>
inline void safeOnLostDevice(T& ptr)
{
    if (ptr)
        ptr->onLostDevice(); 
}
#define SAFE_ON_LOST_DEVICE safeOnLostDevice    // for backward compatiblility

// Safely call onResetDevice
template <typename T>
inline void safeOnResetDevice(T& ptr)
{
    if (ptr)
        ptr->onResetDevice(); 
}
#define SAFE_ON_RESET_DEVICE safeOnResetDevice  // for backward compatiblility

//=============================================================================
//                  Constants
//=============================================================================

// window
const char CLASS_NAME[] = "Collisions";
const char GAME_TITLE[] = "UniPong";
const bool FULLSCREEN = false;              // windowed or fullscreen
const UINT GAME_WIDTH = 550;               // width of game in pixels
const UINT GAME_HEIGHT = 695;               // height of game in pixels

// game
const double PI = 3.14159265;
const float FRAME_RATE = 200.0f;                // the target frame rate (frames/sec)
const float MIN_FRAME_RATE = 10.0f;             // the minimum frame rate
const float MIN_FRAME_TIME = 1.0f/FRAME_RATE;   // minimum desired time for 1 frame
const float MAX_FRAME_TIME = 1.0f/MIN_FRAME_RATE; // maximum time used in calculations
const RECT  COLLISION_RECTANGLE = {-30,-16,30,16};
const RECT  COLLISION_BOX_PADDLE = {-45,-10,45,10};
const RECT COLLISION_BOX_PUCK = {-32, -32, 32, 32};
const float   COLLISION_RADIUS = 29;

// graphic images
const char PUCK_IMAGE[] = "pictures\\PS4_new.png";  // game textures
const char PADDLE_IMAGE[] =     "pictures\\paddle.png";      // menu texture
const char CHECK_IMAGE[] = "pictures\\check.jpg";      // check texture
const char BACK_IMAGE[] = "pictures\\backScreen.png";      // menu texture
const char TITLE_IMAGE[] = "pictures\\title.png";      // menu texture

const char THREE_IMAGE[] = "pictures\\3.png";      // menu texture
const char TWO_IMAGE[] = "pictures\\2.png";      // menu texture
const char ONE_IMAGE[] = "pictures\\1.png";      // menu texture
const char GO_IMAGE[] = "pictures\\go.png";      // menu texture
const char LEVEL1_IMAGE[] = "pictures\\level1.png";      // menu texture
const char LEVEL2_IMAGE[] = "pictures\\level2.png";      // menu texture
const char GAMEOVER_IMAGE[] = "pictures\\gameover.png";      // menu texture
const char YOUWIN_IMAGE[] = "pictures\\youWin.png";      // menu texture

// key mappings
// In this game simple constants are used for key mappings. If variables were used
// it would be possible to save and restore key mappings from a data file.
const UCHAR ESC_KEY      = VK_ESCAPE;       // escape key
const UCHAR ALT_KEY      = VK_MENU;         // Alt key
const UCHAR PADDLE_LEFT    = VK_LEFT;     // left arrow
const UCHAR PADDLE_RIGHT   = VK_RIGHT;    // right arrow
const UCHAR ENTER_KEY    = VK_RETURN; 
const UCHAR PLAYER_LEFT_KEY    = 'A';    // A key
const UCHAR PLAYER_RIGHT_KEY   = 'D';    // D key
const UCHAR PLAYER_UP_KEY      = 'W';    // W key
const UCHAR PLAYER_DOWN_KEY    = 'S';  
//

// audio files required by audio.cpp
// WAVE_BANK must be location of .xwb file.
const char WAVE_BANK[]  = "audio\\Win\\wavebank.xwb";
// SOUND_BANK must be location of .xsb file.
const char SOUND_BANK[] = "audio\\Win\\soundbank.xsb";
const char TEXTURES_IMAGE[] = "pictures\\spaceships.png";
const char CONTROLLS_IMAGE[] = "pictures\\controls.png";
// audio cues
const char AIFIRE[] = "AIfire";
const char BACKGROUNDMUSIC[] = "BackgroundMusic";
const char ENEMYEXPLOSION[] = "EnemyExplosion";
const char ENEMYFIRE[] = "EnemyFire";
const char PLAYEREXPLOSION[]   = "PlayerExplosion";
const char PLAYERFIRE[]   = "PlayerFire";

enum GameStates {intro, readyToPlay,levelOne, levelTwo, GameOver, YouWin, threeState, twoState, oneState, goState,  gamePlay, gamePlay2, end};

#endif
