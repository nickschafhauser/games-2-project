// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.h v1.0

#ifndef _AISHIP_H                 // Prevent multiple definitions if this 
#define _AISHIP_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"
#include <vector>
#include "SchafhauserEnemyBullet.h"
#include <time.h>

namespace AINS
{
    const int WIDTH = 64;                   // image width
    const int HEIGHT = 64;                  // image height
	const int   TEXTURE_COLS = 4;           // texture has 8 columns
	//BARON actions
	const int AI_START = 28;				
	const int AI_END = 28;
	
	const int AI_IDLE_START = 28;				
	const int AI_IDLE_END = 28;
	const float SPEED = 200.0f;                // 100 pixels per second
    const float MASS = 100.0f;         // image height
	const float ROTATION_RATE = (float)PI;

	const int   AI_DESTROYED_START = 8;    // damage start frame
	const int   AI_DESTROYED_END = 23;      // damage end frame

	// TODO MAKE THIS
	const int   AI_HIT_START = 68;    // damage start frame
	const int   AI_HIT_END = 75;      // damage end frame

	const float AI_ANIMATION_DELAY = 0.08f;    // time between frames
	const float AI_IMAGE_SCALE = 1.1f;
}

// inherits from Entity class
class AIShip : public Entity
{
private:
	Entity targetEntity;
	float speed;

	bool hit;
	Image AIHit;

	Image AIDestroyed;

	/*bool AIshipDead;*/
	float timeInState;
	float randomCooldown;
	bool doneFiring;
	
public:
	bool AIshipDead;
	// constructor
	AIShip();
	int health;
	// inherited member functions
    virtual void draw();
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM);
    void update(float frameTime);
	void vectorTrack();
	void ai(float time, Entity &t);

	//void damage(WEAPON);
	void fire(void);
	std::vector<EnemyBullet> bulletVec;
	std::vector<EnemyBullet> bulletsOnScreen;
	bool isDead;
	void setDead(bool d){AIshipDead = d;};
	void recycleBullet(int index);
	void setHit(bool h){hit = h;};
	void setInvisible() {Image::setVisible(false); Entity::setActive(false); isDead = true;} //makes the enemy ship invisible

	void reset(float x, float y);
    //void damage(WEAPON);
};
#endif

