// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.h v1.0

#ifndef _PLAYERSHIP_H                 // Prevent multiple definitions if this 
#define _PLAYERSHIP_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"
#include "Bullet.h"
#include <vector>
#include <deque>

namespace PlayerNS
{
    const int WIDTH = 64;                   // image width
    const int HEIGHT = 64;                  // image height
	const int   TEXTURE_COLS = 4;           // texture has 3 columns
	//BARON actions
	const int PLAYER_START = 0;				
	const int PLAYER_END = 0;
	
	const int PLAYER_IDLE_START = 0;				
	const int PLAYER_IDLE_END = 0;
	const float SPEED = 200.0f;                // 100 pixels per second
    const float MASS = 100.0f;         // image height
	const float ROTATION_RATE = (float)PI;

	const float PLAYER_ANIMATION_DELAY = 0.08f;    // time between frames
	const float PLAYER_IMAGE_SCALE = 1.1f;

	const int   PLAYER_HIT_START = 0;    // damage start frame
	const int   PLAYER_HIT_END = 7;      // damage end frame

	const int   PLAYER_DESTROYED_START = 52;    // damage start frame
	const int   PLAYER_DESTROYED_END = 67;      // damage end frame
}

// inherits from Entity class
class PlayerShip : public Entity
{
private:
	float timeInState;

	bool hit;
	Image playerHit;

	bool dead;
	Image playerDestroyed;

	bool playerDead;
public:
    // constructor
    PlayerShip();
	

    // inherited member functions
    virtual void draw();
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM);
    void update(float frameTime);
    //void damage(WEAPON);
	void fire(void);
	std::vector<Bullet> bulletVec;
	std::deque<Bullet> bulletsOnScreen;
	//bool setHit() {return hit;};
	void setHit(bool h){hit = h;};
	void setDead(bool d){dead = d;};
	bool getDead(){return playerDead;};


	bool isDead;
	int lives; //amount of lives

	void recycleBullet(int index);
	int score;
	void setInvisible() {Image::setVisible(false); Entity::setActive(false); isDead = true;} //makes the enemy ship invisible

	void reset();

	int numberOfEnemiesKilled;

	bool level; //true for level 1, false for level 2.
};
#endif

