// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.h v1.0

#ifndef _SCHAFHAUSERASTEROIDMANAGER_H                 // Prevent multiple definitions if this 
#define _SCHAFHAUSERASTEROIDMANAGER_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"
#include <time.h>
#include "Asteroid.h"
#include <vector>
// inherits from Entity class
class AsteroidManager : public Entity
{
private:
public:
	float timeInState;
    // constructor
    AsteroidManager();

    // inherited member functions
    virtual void draw();
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM);
    void update(float frameTime);
    //void damage(WEAPON);

	void setInvisible() {Image::setVisible(false); Entity::setActive(false);} //makes the enemy ship invisible

	void sendAsteroid();

	std::vector<Asteroid> AsteroidVec;
	std::vector<Asteroid> AsteroidsOnScreen;

	void recycleAsteroid(int index);

	int numberOfAsteroidsUsed;
	int totalAsteroids;

	bool endOfLevel2;

	void reset();
};
#endif

