
              // file is included in more than one place
#define WIN32_LEAN_AND_MEAN
#ifndef SUBMENU_H                 // Prevent multiple definitions if this 
#define SUBMENU_H 

class subMenu;

#include "graphics.h"
#include "constants.h"
#include "textDX.h"
#include <string>
#include <sstream>
#include "input.h"

namespace submenuNS
{ }

// inherits from Entity class
class subMenu
{
private:
   TextDX *menuItemFont;
   TextDX *menuItemFontHighlight;
   TextDX *menuHeadingFont;
   Input   *input;         // pointer to the input system
   Graphics *graphics;
   int selectedItem;

   std::string submenu1;
   std::string submenu2;
   std::string submenu3;

   D3DXVECTOR2 menuAnchor;
   int verticalOffset;
   int linePtr;
   COLOR_ARGB highlightColor ;
   COLOR_ARGB normalColor;
   bool upDepressedLastFrame;
   bool downDepressedLastFrame;

public:
    // constructor
    subMenu();
	void initialize(Graphics *g, Input *i);
	void update();
	int getSelectedItem() {return selectedItem;}
	void displayMenu();
};
#endif

